/*------------Validation Function-----------------*/
 // To count blank fields.


function validate(str) {
	
	pId = document.form.pId.value;
	Title = document.form.Title.value;
	lName = document.form.lName.value;
	fName = document.form.fName.value;
	mName = document.form.mName.value;
	bDate = document.form.bDate.value;
	bPlace = document.form.bPlace.value;
	Gender = document.form.Gender.value;
	cStatus = document.form.cStatus.value;
	Religion = document.form.Religion.value;
	bGroup = document.form.bGroup.value;
	
	
	address = document.form.address.value;
	pMobile = document.form.pMobile.value;
	Email = document.form.Email.value;
	petiType = document.form.petiType.value;
	cName = document.form.cName.value;
	cId = document.form.cId.value;
	pIdent = document.form.pIdent.value;
	
	
	
	if (pId == "") {
		alert("Enter Patient ID");
		document.form.pId.focus();
		return false;
	}
	if (Title == "0") {
		alert("Enter Title");
		document.form.Title.focus();
		return false;
	}
	if (lName == "") {
		alert("Enter Last Name");
		document.form.lName.focus();
		return false;
	}
	if (fName == "") {
		alert("Enter First Name");
		document.form.fName.focus();
		return false;
	}
	if (mName == "") {
		alert("Enter Middle Name");
		document.form.mName.focus();
		return false;
	}
	if (bDate == "") {
		alert("Enter Birthdate");
		document.form.bDate.focus();
		return false;
	}
	if( bPlace == "") {
		alert("Enter Birth Place");
		document.form.bPlace.focus();
		return false;
	}
	if (Gender == "0") {
		alert("Enter Gender");
		document.form.mName.focus();
		return false;
	}
	if (cStatus == "0") {
		alert("Enter Civil Status");
		document.form.cStatus.focus();
		return false;
	}
	if (Religion == "0") {
		alert("Enter Religion Status");
		document.form.Religion.focus();
		return false;
	}
	if (bGroup == "0") {
		alert("Enter Blood Group");
		document.form.bGroup.focus();
		return false;
	}
	
	if (address == "") {
		alert("Enter Address");
		
		document.form.address.focus();
		return false;
	}
	
	if (pMobile == "") {
		alert("Enter Phone No(mobile)");
		document.form.pMobile.focus();
		return false;
	}
	if(Email == ""){
		alert("Enter Email");
		document.form.Email.focus();
		return false;
		
	}
	if(petiType == "0"){
		alert("Enter Patient Type");
		document.forms.petiType.focus();
		return false;
		
	}
	if(cName == ""){
		alert("Enter Company Name");
		document.form.cName.focus;
		return false;
	}
	if(cId == ""){
			alert("Enter Company ID");
			document.form.cId.focus;
			return false;
	}
	if(pIdent == ""){
		alert("Enter Patient Identifier");
		document.forms.pIdent.focus;
		return false;
	}
	else {
		if (str == 'Submit') {
			alert("sure to submit");
			document.form.method = "post";
			document.form.action = 'add_patient';
			document.form.submit();                                                           
		}
	}
}

/*---------------------------------------------------------*/
document.getElementById("first").style.display = "block";
document.getElementById("second").style.display = "none";
//document.getElementById("third").style.display = "none";
/*document.getElementById("fourth").style.display = "none";
document.getElementById("fifth").style.display = "none";*/

// Function that executes on click of first next button.
function next_step1() {
document.getElementById("first").style.display = "none";
document.getElementById("second").style.display = "block";
document.getElementById("active2").style.color = "red";
document.getElementById("active1").style.color = "gray";

}

// Function that executes on click of first previous button.
function prev_step1() {
document.getElementById("first").style.display = "block";
document.getElementById("second").style.display = "none";
document.getElementById("active1").style.color = "red";
document.getElementById("active2").style.color = "gray";
}

/*// Function that executes on click of second next button.
function next_step2() {
document.getElementById("second").style.display = "none";
document.getElementById("third").style.display = "block";
document.getElementById("active3").style.color = "red";
document.getElementById("active2").style.color = "gray";

}

// Function that executes on click of second previous button.
function prev_step2() {
document.getElementById("third").style.display = "none";
document.getElementById("second").style.display = "block";
document.getElementById("active2").style.color = "red";
document.getElementById("active3").style.color = "gray";
}
*/
