package db;

import java.sql.*;

public class DBConnection {
	

	static Connection conn;

	public static Connection getConnection()
	{
		try
		{
	
	
         
            Class.forName("com.mysql.jdbc.Driver");
            try
            {
          	String url = "jdbc:mysql://localhost:3306/imgdemo";
     	 			
                String uname = "root";
               
                 String pwd = "root";
                   
                conn = DriverManager.getConnection(url,uname,pwd);
                System.out.println("connection Successfull");
            }
            catch (SQLException ex)
            {
                ex.printStackTrace();
            }
        }
        catch(ClassNotFoundException e)
        {
            System.out.println(e);
        }
        return conn;
    }

}
