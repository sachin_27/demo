package admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;

import com.mysql.jdbc.Statement;

import db.DBConnection;

/**
 * Servlet implementation class opd_resi
 */
@MultipartConfig(maxFileSize = 16177215)
@WebServlet("/PostImg")
public class PostImg extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String SAVE_DIR="upload/";
	private Connection conn;

	public PostImg() {
		super();
		conn = DBConnection.getConnection();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id = request.getParameter("pid");
		byte[] b = new byte[1024*1024*1];
		String path = "";
		path = request.getParameter("path");
	    File f = new File(path);
		try {
			
			response.setContentType("image/jpeg");
			InputStream is = new FileInputStream(f);
			ServletOutputStream o = response.getOutputStream();
//			if(b!=null)
			o.write(IOUtils.toByteArray(is));
			o.flush();
			o.close();
//			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Unable To Display image");
			System.out.println("Image Display Error=" + e.getMessage());
			return;

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		InputStream inputStream = null;

		// File uploads = new
		// File(getServletContext().getInitParameter("upload.location"));
		
		 String appPath = request.getServletContext().getRealPath("").replace('\\', '/');
 	    
	      System.out.println(appPath);
	      String savePath =appPath + SAVE_DIR;
 	    
		String uploadsDirName = appPath + SAVE_DIR; 
		final Map<String, String> absolutePath = new HashMap<String, String>();
		Collection<Part> parts = request.getParts();
		int i = 0, j = 0;
		for (Part part : parts) {
			String fname =  getFilename(part);
			if(fname!=null){
			File save = new File( fname+ "_" + System.currentTimeMillis());
			String path = save.getAbsolutePath();
			part.write(path);
			absolutePath.put((i++) + "",""+save);
		}
		}

		String pid = request.getParameter("pid");
		
		
		
		try {

			
			PreparedStatement statement;
			
			String filesQuery = "insert into file (pid,path) values";
			for (String s : absolutePath.keySet()) {
				filesQuery += "(" + pid + ",'" +absolutePath.get(s).replace("\\", "/") + "'),";
			}
			statement = conn.prepareStatement(filesQuery.substring(0, filesQuery.length()-1));
			statement.execute();
			
		} catch (Exception ex) {
			System.out.println(ex);
			ex.printStackTrace();
		}

		response.sendRedirect("photo.jsp?pid="+pid);
	}

	private static String getFilename(Part part) {
		// courtesy of BalusC : http://stackoverflow.com/a/2424824/281545
		part.getName();
		for (String cd : part.getHeader("content-disposition").split(";")) {
			if (cd.trim().contains("photo")) {
				String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
				return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE
																													// fix.
			}
		}
		return null;
	}
}
